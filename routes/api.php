<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function(){
    Route::get('countries','SyscountryController@getAllCountry');
    Route::get('provinces','SysprovinceController@getAllProvince');
    Route::get('cities','SysprovinceController@getAllCityByProvince');
    Route::get('sliders','SysSliderController@getAllSlider');
    Route::get('settings','SysSettingController@getSettings');
    Route::get('categories','SysRecipeCategoryController@getAllRecipeCategory');
    Route::post('categories/add','SysRecipeCategoryController@addCategory'); //add new category
    Route::post('sliders/add','SysSliderController@addSlider'); //add new slider
    Route::get('recipes','SysRecipeController@getAllRecipe');
    Route::get('recipes/lastest','SysRecipeController@getAllRecipeNew');
    Route::get('recipes/waiting','SysRecipeController@getAllRecipeStatusWaiting');
    Route::get('recipes/approval','SysRecipeController@getAllRecipeStatusApproval');
    Route::get('recipes/rejected','SysRecipeController@getAllRecipeStatusReject');
    Route::post('recipes/delete','SysRecipeController@deleterecipe');
    Route::post('recipes/update','SysRecipeController@updaterecipe');
    Route::post('recipes/add','SysRecipeController@postRecipe');
    /*
        Add new recipe POST request header
        'Authorization' => token

        Add new recipe POST request body
        'user_id'
        'recipe_name'
        'recipe_category'
        'description'
        'image'
        'gallery'
        'video'
        'inggridients'
        'cookmethd'
        'notes'
        'province'
        'city'
        'status'
        'views'
        'whislist'
        'share'
    */

    Route::get('recipes/member','SysRecipeController@getAllRecipeByMember');
    /*
        Get recipe by user GET request param
        'user_id'
    */

    Route::get('recipes/category','SysRecipeController@getAllRecipeByCategory');
    /*
        Get recipe by category GET request param
        'recipe_category'
    */

    Route::get('recipes/location','SysRecipeController@getAllRecipeByLocation');
    /*
        Get recipe by location GET request param
        'province'
        'city'
    */

    Route::get('recipes/Detail','SysRecipeController@getRecipeDetail');
    /*
        Get recipe detail GET request param
        'recipe_id'
    */
    
    Route::post('recipes/add_wishlist','SysRecipeController@addWishlist');
    /*
        Add new recipe POST request header
        'Authorization' => token

        Add new recipe POST request body
        'user_id'
        'recipe_id'
    */

     
    Route::post('recipes/delete_wishlist','SysRecipeController@deleteWishlist');
    /*
        Add new recipe POST request header
        'Authorization' => token

        Add new recipe POST request body
        'user_id'
        'recipe_id'
    */

    Route::get('recipes/get_wishlist','SysRecipeController@getWishlist');
    /*
        Get recipe detail GET request param
        'user_id'
    */

    Route::get('recipes/search','SysRecipeController@searchRecipe');
    /*
        Get recipe detail GET request param
        'string'
    */
    
    Route::post('recipes/add_review','SysRecipeController@addReview');
    /*
        Add new recipe POST request header
        'Authorization' => token

        Add new recipe POST request body
        'user_id'
        'recipe_id'
        'rating'
        'comment'
    */

    Route::get('recipes/get_review','SysRecipeController@getReview');
    /*
        Get recipe detail GET request param
        'recipe_id'
    */


    Route::get('recipes/get_rating','SysRecipeController@getTotalRating');
    /*
        Get recipe detail GET request param
        'recipe_id'
    */

    Route::post('account/register','SysMembersController@register');
    Route::post('account/login','SysMembersController@login');
    Route::get('account/get_user','SysMembersController@getUserById');

    Route::post('account/update','SysMembersController@update');
    /*
        Update member data POST request header
        'Authorization' => token

        Update member data POST request body
        "user_id"
        "fullname"
        "email"
        "phone"
        "address"
        "gender"
        "avatar"
    */

    Route::post('account/change/password','SysMembersController@changepassword');
    /*
        Update member data POST request header
        'Authorization' => token

        Update member data POST request body
        "user_id"
        "email"
        "password"
    */
    
    Route::post('account/logout','SysMembersController@logout');
    /*
        Logout POST request header
        'Authorization' => token

        Logout POST request body
        "user_id"
    */

    Route::group(['prefix' => 'admin'], function(){
        Route::post('register','SysUserController@register');
        Route::post('login','SysUserController@login');
        Route::post('logout','SysUserController@logout'); //logout
        Route::get('dashboard', 'SysUserController@getDashboardData');
        
        Route::get('categories','SysRecipeCategoryController@getAllRecipeCategoryAdmin');
        Route::post('categories/edit','SysRecipeCategoryController@editCategory');
        Route::post('categories/change_status','SysRecipeCategoryController@changeCategoryStatus');
        Route::post('categories/upload_image', 'SysRecipeCategoryController@uploadCategoryImage');
        
        Route::get('recipes','SysRecipeController@getAllRecipeAdmin');
        Route::post('recipes/change_status','SysRecipeController@changeRecipeStatus');

        Route::get('members','SysMembersController@getUserAdmin');
        Route::post('members/change_status','SysMembersController@changeMemberStatus');

        Route::get('sliders','SysSliderController@getAllSliderAdmin');
        Route::post('sliders/edit','SysSliderController@editSlider');
        Route::post('sliders/change_status','SysSliderController@changeSliderStatus');
        Route::post('sliders/upload_image', 'SysSliderController@uploadSliderImage');

    });
});