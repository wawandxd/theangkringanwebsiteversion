<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysToken extends Model
{
    protected $table = 'sys_token';

    protected $fillable = ['user_id', 'user_type', 'token'];

}
