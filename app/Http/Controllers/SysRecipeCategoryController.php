<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SysCategoryRecipe;
use App\SysToken;
use DB;
use DateTime;

class SysRecipeCategoryController extends Controller
{
    public function getAllRecipeCategory(){
        $categoryrecipelist = SysCategoryRecipe::where('status', '=', 1)->where('deleted_at','=',NULL)->get();
        
        if(count($categoryrecipelist) > 0 )
        {
            $status       = true;
            $status_code  = 200;
            $message      = "data found";
            $data         = $categoryrecipelist;
        }else {
            $status       = false;
            $status_code  = 404;
            $message      = "data not found";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data];

        return response()->json($resp);
    }

    public function getAllRecipeCategoryAdmin(){
        $categoryrecipelist = SysCategoryRecipe::where('deleted_at','=',NULL)->get();
        
        if(count($categoryrecipelist) > 0 )
        {
            $status       = true;
            $status_code  = 200;
            $message      = "data found";
            $data         = $categoryrecipelist;
        }else {
            $status       = false;
            $status_code  = 404;
            $message      = "data not found";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data];

        return response()->json($resp);
    }
    
    public function addCategory(Request $request){
        
        $token = $request->header('Authorization');

        if($token == ""){
            $status       = false;
            $status_code  = 401;
            $message      = "Unauthorized";
            $data         = null;
        }else{
            $userid = $request->input('user_id');
    
            $chcktoken = SysToken::where('user_id', '=', $userid)
                                ->where('token', '=', $token)
                                ->get();
    
            if(count($chcktoken) > 0){
                $category_name	= $request->input("category_name");
                $description      = $request->input("description");
                $image	    = $request->input("image");
                $category_status     = 1;
            
                if($category_name == "" || $description == ""){
                    $status       = false;
                    $status_code  = 402;
                    $message      = "Field can not be null";
                    $data         = null;
                }else{
                    $chckdata = SysCategoryRecipe::where('category_name', '=', $category_name)->get();
                    if(count($chckdata) > 0){
                        $status       = false;
                        $status_code  = 400;
                        $message      = "Category Already Exist";
                        $data         = null;
                    }else {
                        $category = SysCategoryRecipe::create([
                            'category_name' => $category_name,
                            'description' => $description,
                            'image' => $image,
                            'status' => $category_status,
                        ]);
                        DB::commit();

                        $retdata = array(
                            'category_id'	=> $category->id,
                            'category_name' => $category_name,
                            'description' => $description,
                            'image' => $image,
                        );
            
                        $status       = true;
                        $status_code  = 200;
                        $message      = "data found";
                        $data         = $retdata;
                    }
                }
            }else{
                $status       = false;
                $status_code  = 401;
                $message      = "Unauthenticated";
                $data         = null;
            }

        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);
    }
    
    public function changeCategoryStatus(Request $request){
        $token = $request->header('Authorization');

        if($token == ""){
            $status       = false;
            $status_code  = 401;
            $message      = "Unauthorized";
            $data         = null;
        }else{
            $userid = $request->input('user_id');
    
            $chcktoken = SysToken::where('user_id', '=', $userid)
                                ->where('token', '=', $token)
                                ->where('user_type', '=', 'admin')
                                ->get();
    
            if(count($chcktoken) > 0){
                $categoryid = $request->input('category_id');
    
                $category = SysCategoryRecipe::find($categoryid);
                if($category->status == 1){
                    $category->status = 0;
                }else{
                    $category->status = 1;
                }

                if($category->save()){
                    $status       = true;
                    $status_code  = 200;
                    $message      = "category disabled successfully";
                    $data         = null;
                }else{
                
                    $status       = false;
                    $status_code  = 400;
                    $message      = "failed to disable category";
                    $data         = null;
                }
            }else{
                $status       = false;
                $status_code  = 401;
                $message      = "Unauthenticated";
                $data         = null;
            }

        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);
    }
    
    public function editCategory(Request $request){
        $token = $request->header('Authorization');

        if($token == ""){
            $status       = false;
            $status_code  = 401;
            $message      = "Unauthorized";
            $data         = null;
        }else{
            $userid = $request->input('user_id');
    
            $chcktoken = SysToken::where('user_id', '=', $userid)
                                ->where('token', '=', $token)
                                ->where('user_type', '=', 'admin')
                                ->get();
    
            if(count($chcktoken) > 0){
                $categoryid = $request->input('id');
                $categoryname = $request->input('category_name');
                $categorydescription = $request->input('description');
                $categoryimage = $request->input('image');
    
                $category = SysCategoryRecipe::find($categoryid);
                $category->category_name = $categoryname;
                $category->description = $categorydescription;
                $category->image = $categoryimage;

                if($category->save()){
                    $status       = true;
                    $status_code  = 200;
                    $message      = "category edited successfully";
                    $data         = null;
                }else{
                
                    $status       = false;
                    $status_code  = 400;
                    $message      = "failed to edit category";
                    $data         = null;
                }
            }else{
                $status       = false;
                $status_code  = 401;
                $message      = "Unauthenticated";
                $data         = null;
            }

        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);
    }

    public function uploadCategoryImage(Request $request){

        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('file');
 
                  // isi dengan nama folder tempat kemana file diupload

        $tujuan_upload = public_path().'/assets/apiv1/category/';
 
                // upload file
        if($file->move($tujuan_upload,$file->getClientOriginalName())){
            $status       = true;
            $status_code  = 200;
            $message      = "image uploaded";
            $data         = [
                "image" => $file->getClientOriginalName()
            ];
        }else{
        
            $status       = false;
            $status_code  = 400;
            $message      = "failed to upload image";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);

    }
}
