<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\SysCountry;

class SyscountryController extends Controller
{
    public function getAllCountry(){
        $countrylist = SysCountry::all();
        
        if(count($countrylist) > 0 )
          {
            $status       = true;
            $status_code  = 200;
            $message      = "data found";
            $data         = $countrylist;
          }else {
            $status       = false;
            $status_code  = 201;
            $message      = "data not found";
            $data         = null;
          }

          $resp         =['status' => $status,
                          'status_code' => $status_code,
                          'message' => $message,
                          'data'  => $data];

        return response()->json($resp);
    }
}
