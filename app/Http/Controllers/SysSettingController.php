<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SysSetting;

class SysSettingController extends Controller
{
    public function getSettings(){
        $settings = SysSetting::all();
        
        if(count($settings) > 0 )
        {
            $status       = true;
            $status_code  = 200;
            $message      = "data found";
            $data         = $settings;
        }else {
            $status       = false;
            $status_code  = 404;
            $message      = "data not found";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data];

        return response()->json($resp);
    }
}
