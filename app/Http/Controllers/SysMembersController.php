<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SysMembers;
use App\SysToken;
use DB;
use DateTime;

class SysMembersController extends Controller
{
    public function register(Request $request) 
    {
        $fullname	= $request->input("fullname");
        $email      = $request->input("email");
        $phone	    = $request->input("phone");
        $address	= $request->input("address");
        $gender	    = $request->input("gender");
        $passwd     = $request->input("password");
        $now        = new DateTime();
        $status     = 1;
    
        $chkdata = SysMembers::where('email','=', $email)->get();
    
        if($fullname == "" || $email == "" || $phone == "" || $address == "" || $gender == "")
        {
            $status       = false;
            $status_code  = 402;
            $message      = "Field can not be null";
            $data         = null;
        }
        else
        {
            if(count($chkdata) > 0){
                $status       = false;
                $status_code  = 400;
                $message      = "Email Already Exist";
                $data         = null;
            }else {
                $pass = MD5($passwd);
                $sysid = SysMembers::create([
                    'fullname' => $fullname,
                    'email' => $email,
                    'phone' => $phone,
                    'address' => $address,
                    'gender' => $gender,
                    'password' => $pass,
                    'role_id' => '5',
                    'avatar' => 'https://picsum.photos/200',
                    'status' => '1',
                ]);
                DB::commit();
    
                function generateRandomString($length = 20) {
                    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $charactersLength = strlen($characters);
                    $randomString = '';
                    for ($i = 0; $i < $length; $i++) {
                       $randomString .= $characters[rand(0, $charactersLength - 1)];
                    }
                    return $randomString;
                }
    
                $tokn = generateRandomString(20);
                $tokenlog = $tokn.time();

                $chcktoken = SysToken::where('user_id', '=', $sysid->id)
                                    ->where('user_type', '=', 'member')
                                    ->get();
    
                if(count($chcktoken) > 0){
                    $user_token = SysToken::find($chcktoken[0]->id);
                    $user_token->token = $tokenlog;
                    $user_token->save();
                }else{
                    SysToken::create([
                        'user_id' => $sysid->id,
                        'user_type' => 'member',
                        'token' => $tokenlog
                    ]);
                    DB::commit();
                }

                $retdata = array(
                    'user_id'	=> $sysid->id,
                    'name' => $fullname,
                    'email' => $email,
                    'avaimg' => $sysid->avatar,
                    'token' =>  $tokenlog
                );
    
                $status       = true;
                $status_code  = 200;
                $message      = "data found";
                $data         = $retdata;
            }
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);
    }

    public function login(Request $request) {
		$email	= $request->input("email");
        $passwd = $request->input("password");
        $pass = MD5($passwd);
        $now = new DateTime();

        $chkdata = DB::table('sys_members')
                ->where('email','=', $email)
                ->where('password','=', $pass)
                ->where('role_id','=', '5')
                ->get();

        if(count($chkdata) > 0)
        {
            if($chkdata[0]->status == 1){
                $userid = $chkdata[0]->id;
                $fullname = $chkdata[0]->fullname;
                $email = $chkdata[0]->email;
                $avatar = $chkdata[0]->avatar;
    
                function generateRandomString($length = 20) {
                       $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                       $charactersLength = strlen($characters);
                       $randomString = '';
                       for ($i = 0; $i < $length; $i++) {
                           $randomString .= $characters[rand(0, $charactersLength - 1)];
                       }
                       return $randomString;
                }
                    
                $tokn = generateRandomString(20);
                $tokenlog = $tokn.time();
    
                $chcktoken = SysToken::where('user_id', '=', $userid)
                                    ->where('user_type', '=', 'member')
                                    ->get();
    
                if(count($chcktoken) > 0){
                    $user_token = SysToken::find($chcktoken[0]->id);
                    $user_token->token = $tokenlog;
                    $user_token->save();
                }else{
                    SysToken::create([
                        'user_id' => $userid,
                        'user_type' => 'member',
                        'token' => $tokenlog
                    ]);
                    DB::commit();
                }
    
                $retdata = array(
                    'user_id'	=> $userid,
                    'name' => $fullname,
                    'email' => $email,
                    'avaimg' => $avatar,
                    'token' =>  $tokenlog
                );
    
                $status       = true;
                $status_code  = 200;
                $message      = "data found";
                $data         = $retdata;
            }else{
    
                $status       = false;
                $status_code  = 400;
                $message      = "Account banned. Please contact administrator";
                $data         = null;

            }
        }
        else
        {
            $status       = false;
            $status_code  = 401;
            $message      = "Invalid username or password";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);
    }

    public function update(Request $request) {
        
        $token = $request->header('Authorization');

        if($token == ""){
            $status       = false;
            $status_code  = 401;
            $message      = "Unauthorized";
            $data         = null;
        }else{
            $userid = $request->input('user_id');
    
            $chcktoken = SysToken::where('user_id', '=', $userid)
                                ->where('user_type', '=', 'member')
                                ->where('token', '=', $token)
                                ->get();
    
            if(count($chcktoken) > 0){
                $member_id	= $request->input("user_id");
                $fullname	= $request->input("fullname");
                $email      = $request->input("email");
                $phone	    = $request->input("phone");
                $address	= $request->input("address");
                $gender	    = $request->input("gender");
                $avatar     = $request->file("avatar");

                $member = SysMembers::find($member_id);

                if($member)
                {
                    if($avatar != "")
                    {
                        $upload = env('UPLOAD_PATH_MEMBER');
                        $image_name = time()."."."jpg";
                        $success = $avatar->move($upload,$image_name);
                        $IMGurl = 'https://theangkringan.harmonydshine.web.id/assets/apiv1/member/'.$image_name;
                    }else{
                        $IMGurl = $member->avatar;
                    }

                    $member->fullname = $fullname;
                    $member->email = $email;
                    $member->phone = $phone;
                    $member->address = $address;
                    $member->gender = $gender;
                    $member->avatar = $IMGurl;

                    if($member->save()){    

                        $retdata = array(
                            'user_id'	=> $request->input('user_id')
                        );

                        $status       = true;
                        $status_code  = 200;
                        $message      = "Data updated successfully ";
                        $data         = $retdata;
                    }
                }
                else
                {
                    $status       = false;
                    $status_code  = 404;
                    $message      = "Data not found";
                    $data         = null;
                }
            }else{
                $status       = false;
                $status_code  = 401;
                $message      = "Unauthenticated";
                $data         = null;
            }

        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);
    }

    public function logout(Request $request){
        $token = $request->header('Authorization');

        if($token == ""){
            $status       = false;
            $status_code  = 401;
            $message      = "Unauthorized";
            $data         = null;
        }else{
            $userid = $request->input('user_id');
    
            $chcktoken = SysToken::where('user_id', '=', $userid)
                                ->where('user_type', '=', 'member')
                                ->where('token', '=', $token)
                                ->get();
    
            if(count($chcktoken) > 0){
                if(SysToken::destroy($chcktoken[0]->id)){
                    $status       = true;
                    $status_code  = 200;
                    $message      = "Logout successfully";
                    $data         = null;
                }else{
                    $status       = false;
                    $status_code  = 400;
                    $message      = "Logout failed. Please try again";
                    $data         = null;
                }
            }else{
                $status       = false;
                $status_code  = 401;
                $message      = "Unauthenticated";
                $data         = null;
            }

        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);

    }

    public function getUserById(Request $request){
        $token = $request->header('Authorization');

        if($token == ""){
            $status       = false;
            $status_code  = 401;
            $message      = "Unauthorized";
            $data         = null;
        }else{
            $userid = $request->input('user_id');
    
            $chcktoken = SysToken::where('user_id', '=', $userid)
                                ->where('user_type', '=', 'member')
                                ->where('token', '=', $token)
                                ->get();
    
            if(count($chcktoken) > 0){ 
                $member = SysMembers::where('id', '=', $request->input('user_id'))->where('status', '=', '1')->get();
                if(count($member) > 0 )
                {
                    $status       = true;
                    $status_code  = 200;
                    $message      = "data found";
                    $data         = $member;
                }else {
                    $status       = false;
                    $status_code  = 404;
                    $message      = "data not found";
                    $data         = null;
                }
            }else{
                $status       = false;
                $status_code  = 401;
                $message      = "Unauthenticated";
                $data         = null;
            }
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data];

        return response()->json($resp);
    }

    public function getUserAdmin(Request $request){
        $member = SysMembers::where('deleted_at', '=', null)->get();
        
        if(count($member) > 0 )
        {
            $status       = true;
            $status_code  = 200;
            $message      = "data found";
            $data         = $member;
        }else {
            $status       = false;
            $status_code  = 404;
            $message      = "data not found";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data];

        return response()->json($resp);
    }
    
    public function changeMemberStatus(Request $request){
        $token = $request->header('Authorization');

        if($token == ""){
            $status       = false;
            $status_code  = 401;
            $message      = "Unauthorized";
            $data         = null;
        }else{
            $userid = $request->input('user_id');
    
            $chcktoken = SysToken::where('user_id', '=', $userid)
                                ->where('token', '=', $token)
                                ->where('user_type', '=', 'admin')
                                ->get();
    
            if(count($chcktoken) > 0){
                $memberid = $request->input('member_id');
    
                $member = SysMembers::find($memberid);
                if($member->status == 1){
                    $member->status = 0;
                }else{
                    $member->status = 1;
                }

                if($member->save()){
                    $status       = true;
                    $status_code  = 200;
                    $message      = "Account status changed";
                    $data         = null;
                }else{
                
                    $status       = false;
                    $status_code  = 400;
                    $message      = "failed to change account status";
                    $data         = null;
                }
            }else{
                $status       = false;
                $status_code  = 401;
                $message      = "Unauthenticated";
                $data         = null;
            }

        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);
    }

    public function changepassword(Request $request)
    {
        $email = $request->input("email");
        $newpasswd = $request->input("password");
        $pass = MD5($newpasswd);

        $member = SysMembers::where('email',$email)->first();

                if($member)
                {
                    $member->password = $pass;

                    if($member->save()){    

                        $retdata = array(
                            'user_id'	=>$member->user_id,
                            'email'	=> $request->input('email')
                        );

                        $status       = true;
                        $status_code  = 200;
                        $message      = "Change password successfully ";
                        $data         = $retdata;
                    }
                }
                else
                {
                    $status       = false;
                    $status_code  = 404;
                    $message      = "Data not found";
                    $data         = null;
                }


        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);
    }
}
