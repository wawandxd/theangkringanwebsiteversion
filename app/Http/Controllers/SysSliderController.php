<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SysSlider;
use App\SysToken;
use DB;
use DateTime;

class SysSliderController extends Controller
{
    public function getAllSlider(){
        $sliderlist = SysSlider::where('status', 1)->where('deleted_at','=',NULL)->get();
        
        if(count($sliderlist) > 0 )
        {
            $status       = true;
            $status_code  = 200;
            $message      = "data found";
            $data         = $sliderlist;
        }else {
            $status       = false;
            $status_code  = 404;
            $message      = "data not found";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data];

        return response()->json($resp);
    }

    public function getAllSliderAdmin(){
        $sliderlist = SysSlider::where('sys_slider.deleted_at','=',NULL)->get();
        
        if(count($sliderlist) > 0 )
        {
            $status       = true;
            $status_code  = 200;
            $message      = "data found";
            $data         = $sliderlist;
        }else {
            $status       = false;
            $status_code  = 404;
            $message      = "data not found";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data];

        return response()->json($resp);
    }
    
    public function addSlider(Request $request){
        
        $token = $request->header('Authorization');

        if($token == ""){
            $status       = false;
            $status_code  = 401;
            $message      = "Unauthorized";
            $data         = null;
        }else{
            $userid = $request->input('user_id');
    
            $chcktoken = SysToken::where('user_id', '=', $userid)
                                ->where('token', '=', $token)
                                ->get();
    
            if(count($chcktoken) > 0){
                $title	= $request->input("title");
                $subtitle      = $request->input("subtitle");
                $image	    = $request->input("image");
                $link	    = $request->input("link");
                $url	    = $request->input("url");
                $status     = 1;
            
                if($title == "" || $subtitle == "" || $image == "" || $link == "" || $url == ""){
                    $status       = false;
                    $status_code  = 402;
                    $message      = "Field can not be null";
                    $data         = null;
                }else{
                    $chckdata = SysSlider::where('title', '=', $title)->get();
                    if(count($chckdata) > 0){
                        $status       = false;
                        $status_code  = 400;
                        $message      = "Slider Already Exist";
                        $data         = null;
                    }else {
                        $slider = SysSlider::create([
                            'title' => $title,
                            'subtitle' => $subtitle,
                            'image' => $image,
                            'link' => $link,
                            'url' => $url,
                            'status' => $status,
                        ]);
                        DB::commit();

                        $retdata = array(
                            'slider_id'	=> $slider->id,
                            'title' => $title,
                            'subtitle' => $subtitle,
                            'image' => $image,
                        );
            
                        $status       = true;
                        $status_code  = 200;
                        $message      = "data found";
                        $data         = $retdata;
                    }
                }
            }else{
                $status       = false;
                $status_code  = 401;
                $message      = "Unauthenticated";
                $data         = null;
            }

        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);
    }
    
    public function editSlider(Request $request){
        $token = $request->header('Authorization');

        if($token == ""){
            $status       = false;
            $status_code  = 401;
            $message      = "Unauthorized";
            $data         = null;
        }else{
            $userid = $request->input('user_id');
    
            $chcktoken = SysToken::where('user_id', '=', $userid)
                                ->where('token', '=', $token)
                                ->where('user_type', '=', 'admin')
                                ->get();
    
            if(count($chcktoken) > 0){
                $id = $request->input('id');
                $title = $request->input('title');
                $subtitle = $request->input('subtitle');
                $image = $request->input('image');
                $link = $request->input('link');
                $url = $request->input('url');
    
                $slider = SysSlider::find($id);
                $slider->title = $title;
                $slider->subtitle = $subtitle;
                $slider->image = $image;
                $slider->link = $link;
                $slider->url = $url;

                if($slider->save()){
                    $status       = true;
                    $status_code  = 200;
                    $message      = "slider edited successfully";
                    $data         = null;
                }else{
                
                    $status       = false;
                    $status_code  = 400;
                    $message      = "failed to edit slider";
                    $data         = null;
                }
            }else{
                $status       = false;
                $status_code  = 401;
                $message      = "Unauthenticated";
                $data         = null;
            }

        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);
    }
    
    public function changeSliderStatus(Request $request){
        $token = $request->header('Authorization');

        if($token == ""){
            $status       = false;
            $status_code  = 401;
            $message      = "Unauthorized";
            $data         = null;
        }else{
            $userid = $request->input('user_id');
    
            $chcktoken = SysToken::where('user_id', '=', $userid)
                                ->where('token', '=', $token)
                                ->where('user_type', '=', 'admin')
                                ->get();
    
            if(count($chcktoken) > 0){
                $sliderid = $request->input('slider_id');
    
                $slider = SysSlider::find($sliderid);
                if($slider->status == 1){
                    $slider->status = 0;
                }else{
                    $slider->status = 1;
                }

                if($slider->save()){
                    $status       = true;
                    $status_code  = 200;
                    $message      = "slider disabled successfully";
                    $data         = null;
                }else{
                
                    $status       = false;
                    $status_code  = 400;
                    $message      = "failed to disable slider";
                    $data         = null;
                }
            }else{
                $status       = false;
                $status_code  = 401;
                $message      = "Unauthenticated";
                $data         = null;
            }

        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);
    }

    public function uploadSliderImage(Request $request){

        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('file');
 
        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = public_path().'/assets/apiv1/slider/';
        // $tujuan_upload = public_path().'/admin/img/category/';
 
                // upload file
        if($file->move($tujuan_upload,$file->getClientOriginalName())){
            $status       = true;
            $status_code  = 200;
            $message      = "image uploaded";
            $data         = [
                "image" => $file->getClientOriginalName()
            ];
        }else{
        
            $status       = false;
            $status_code  = 400;
            $message      = "failed to upload image";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);

    }
}
