<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SysUser;
use App\SysToken;
use App\SysCategoryRecipe;
use App\SysMembers;
use App\SysRecipe;
use DB;
use DateTime;

class SysUserController extends Controller
{
    public function register(Request $request) 
    {
        $fullname	= $request->input("fullname");
        $email      = $request->input("email");
        $phone	    = $request->input("phone");
        $address	= $request->input("address");
        $gender	    = $request->input("gender");
        $passwd     = $request->input("password");
        $now        = new DateTime();
        $status     = 1;
    
        $chkdata = SysUser::where('email','=', $email)->get();
    
        if($fullname == "" || $email == "" || $phone == "" || $address == "" || $gender == "")
        {
            $status       = false;
            $status_code  = 402;
            $message      = "Field can not be null";
            $data         = null;
        }
        else
        {
            if(count($chkdata) > 0){
                $status       = false;
                $status_code  = 400;
                $message      = "Email Already Exist";
                $data         = null;
            }else {
                $pass = MD5($passwd);
                $sysid = SysUser::create([
                    'fullname' => $fullname,
                    'email' => $email,
                    'phone' => $phone,
                    'address' => $address,
                    'gender' => $gender,
                    'password' => $pass,
                    'role_id' => '2',
                    'avatar' => 'https://picsum.photos/200',
                    'status' => '1',
                ]);
                DB::commit();
    
                function generateRandomString($length = 20) {
                    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $charactersLength = strlen($characters);
                    $randomString = '';
                    for ($i = 0; $i < $length; $i++) {
                       $randomString .= $characters[rand(0, $charactersLength - 1)];
                    }
                    return $randomString;
                }
    
                $tokn = generateRandomString(20);
                $tokenlog = $tokn.time();

                $retdata = array(
                    'user_id'	=> $sysid->id,
                    'name' => $fullname,
                    'email' => $email,
                    'avaimg' => $sysid->avatar,
                    'token' =>  $tokenlog
                );
    
                $status       = true;
                $status_code  = 200;
                $message      = "data found";
                $data         = $retdata;
            }
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);
    }

    public function login(Request $request) {
		$email	= $request->input("email");
        $passwd = $request->input("password");
        $pass = MD5($passwd);
        $now = new DateTime();

        $chkdata = SysUser::where('email','=', $email)
                ->where('password','=', $pass)
                ->where('role_id','=', '2')
                ->get();

        if(count($chkdata) > 0)
        {
            $userid = $chkdata[0]->id;
            $fullname = $chkdata[0]->fullname;
            $email = $chkdata[0]->email;
            $avatar = $chkdata[0]->avatar;

            function generateRandomString($length = 20) {
         	      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
           	    $charactersLength = strlen($characters);
           	    $randomString = '';
           	    for ($i = 0; $i < $length; $i++) {
           	        $randomString .= $characters[rand(0, $charactersLength - 1)];
           	    }
           	    return $randomString;
            }
                
            $tokn = generateRandomString(20);
            $tokenlog = $tokn.time();

            $chcktoken = SysToken::where('user_id', '=', $userid)
                                ->where('user_type', '=', 'admin')
                                ->get();

            if(count($chcktoken) > 0){
                $user_token = SysToken::find($chcktoken[0]->id);
                $user_token->token = $tokenlog;
                $user_token->save();
            }else{
                SysToken::create([
                    'user_id' => $userid,
                    'user_type' => 'admin',
                    'token' => $tokenlog
                ]);
                DB::commit();
            }

            $retdata = array(
                'user_id'	=> $userid,
                'name' => $fullname,
                'email' => $email,
                'avaimg' => $avatar,
                'token' =>  $tokenlog
            );

            $status       = true;
            $status_code  = 200;
            $message      = "data found";
            $data         = $retdata;
        }
        else
        {
            $status       = false;
            $status_code  = 401;
            $message      = "Invalid username or password";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);
    }

    public function logout(Request $request){
        $token = $request->header('Authorization');

        if(!$token || $token == ""){
            $status       = false;
            $status_code  = 401;
            $message      = "Unauthorized";
            $data         = null;
        }else{
            $userid = $request->input('user_id');
    
            $chcktoken = SysToken::where('user_id', '=', $userid)
                                ->where('user_type', '=', 'admin')
                                ->where('token', '=', $token)
                                ->get();
    
            if(count($chcktoken) > 0){
                if(SysToken::destroy($chcktoken[0]->id)){
                    $status       = true;
                    $status_code  = 200;
                    $message      = "Logout successfully";
                    $data         = null;
                }else{
                    $status       = false;
                    $status_code  = 400;
                    $message      = "Logout failed. Please try again";
                    $data         = null;
                }
            }else{
                $status       = false;
                $status_code  = 401;
                $message      = "Unauthenticated";
                $data         = null;
            }

        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);

    }

    public function getDashboardData(){
        $category = SysCategoryRecipe::where('status', 1)->where('deleted_at', null)->count();
        $recipe = SysRecipe::where('status', 2)->where('deleted_at', null)->count();
        $member = SysMembers::where('status', 1)->where('deleted_at', null)->count();

        $retdata = array(
            'category'	=> $category,
            'recipe' => $recipe,
            'member' => $member
        );

        $status       = true;
        $status_code  = 200;
        $message      = "data found";
        $data         = $retdata;

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);
    }
}
