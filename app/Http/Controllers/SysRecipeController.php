<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SysRecipe;
use App\SysToken;
use App\SysWishlist;
use App\SysMembers;
use App\SysReview;
use DateTime;
use DB;
use Mail;

class SysRecipeController extends Controller
{
    public function getAllRecipe(){
        $recipelist = SysRecipe::where('status', '=', 2)->where('deleted_at','=',NULL)->get();
        
        if(count($recipelist) > 0 )
        {
            $status       = true;
            $status_code  = 200;
            $message      = "data found";
            $data         = $recipelist;
        }else {
            $status       = false;
            $status_code  = 404;
            $message      = "data not found";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data];

        return response()->json($resp);
    }

    public function getAllRecipeAdmin(){
        $recipelist = SysRecipe::join('sys_members', 'sys_members.id', '=', 'sys_recipe.user_id')
                                ->join('sys_category_recipe', 'sys_category_recipe.id', '=', 'sys_recipe.recipe_category')
                                ->join('master_province', 'master_province.id', '=', 'sys_recipe.province')
                                ->join('master_city', 'master_city.id', '=', 'sys_recipe.city')
                                ->select(
                                    'sys_recipe.id AS recipe_id',
                                    'sys_recipe.recipe_name',
                                    'sys_category_recipe.id AS category_id',
                                    'sys_category_recipe.category_name',
                                    'sys_recipe.description',
                                    'sys_recipe.image',
                                    'sys_recipe.video',
                                    'sys_recipe.gallery',
                                    'sys_recipe.inggridients',
                                    'sys_recipe.cookmethd',
                                    'master_province.id AS province_id',
                                    'master_province.name AS province',
                                    'master_city.id AS city_id',
                                    'master_city.name AS city',
                                    'sys_members.id AS user_id',
                                    'sys_members.fullname',
                                    'sys_recipe.status'
                                )
                                ->where('sys_recipe.deleted_at','=',NULL)->get();
        
        if(count($recipelist) > 0 )
        {
            $status       = true;
            $status_code  = 200;
            $message      = "data found";
            $data         = $recipelist;
        }else {
            $status       = false;
            $status_code  = 404;
            $message      = "data not found";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data];

        return response()->json($resp);
    }

    public function getAllRecipeByMember(Request $request){
        $recipelist = SysRecipe::where('user_id', '=', $request->input('user_id'))
                                ->where('deleted_at','=',NULL)
                                ->get();
        
        if(count($recipelist) > 0 )
        {
            $status       = true;
            $status_code  = 200;
            $message      = "data found";
            $data         = $recipelist;
        }else {
            $status       = false;
            $status_code  = 404;
            $message      = "data not found";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data];

        return response()->json($resp);
    }

    public function getAllRecipeByCategory(Request $request){
        $recipelist = SysRecipe::where('recipe_category', '=', $request->input('recipe_category'))
                                ->where('status', '=', 2)
                                ->where('deleted_at','=',NULL)
                                ->get();
        
        if(count($recipelist) > 0 )
        {
            $status       = true;
            $status_code  = 200;
            $message      = "data found";
            $data         = $recipelist;
        }else {
            $status       = false;
            $status_code  = 404;
            $message      = "data not found";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data];

        return response()->json($resp);
    }

    public function getAllRecipeStatusWaiting(Request $request){
        $recipelist = SysRecipe::where('user_id', '=', $request->input('user_id'))
                                ->where('status', '=', 1)
                                ->where('deleted_at','=',NULL)
                                ->get();
        
        if(count($recipelist) > 0 )
        {
            $status       = true;
            $status_code  = 200;
            $message      = "data found";
            $data         = $recipelist;
        }else {
            $status       = false;
            $status_code  = 404;
            $message      = "data not found";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data];

        return response()->json($resp);
    }

    public function getAllRecipeStatusApproval(Request $request){
        $recipelist = SysRecipe::where('user_id', '=', $request->input('user_id'))
                                ->where('status', '=', 2)
                                ->where('deleted_at','=',NULL)
                                ->get();
        
        if(count($recipelist) > 0 )
        {
            $status       = true;
            $status_code  = 200;
            $message      = "data found";
            $data         = $recipelist;
        }else {
            $status       = false;
            $status_code  = 404;
            $message      = "data not found";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data];

        return response()->json($resp);
    }

    public function getAllRecipeStatusReject(Request $request){
        $recipelist = SysRecipe::where('user_id', '=', $request->input('user_id'))
                                ->where('status', '=', 3)
                                ->where('deleted_at','=',NULL)
                                ->get();
        
        if(count($recipelist) > 0 )
        {
            $status       = true;
            $status_code  = 200;
            $message      = "data found";
            $data         = $recipelist;
        }else {
            $status       = false;
            $status_code  = 404;
            $message      = "data not found";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data];

        return response()->json($resp);
    }

    public function getAllRecipeByLocation(Request $request){
        if($request->input('city') == ''){
            $recipelist = SysRecipe::where('deleted_at','=',NULL)
                                    ->where('status', '=', 2)
                                    ->where('province', '=', $request->input('province'))
                                    ->get();
        }else{
            $recipelist = SysRecipe::where('deleted_at','=',NULL)
                                    ->where('status', '=', 2)
                                    ->where('province', '=', $request->input('province'))
                                    ->where('city', '=', $request->input('city'))
                                    ->get();
        }
        
        if(count($recipelist) > 0 )
        {
            $status       = true;
            $status_code  = 200;
            $message      = "data found";
            $data         = $recipelist;
        }else {
            $status       = false;
            $status_code  = 404;
            $message      = "data not found";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data];

        return response()->json($resp);
    }

    public function getAllRecipeNew(){
        $recipelistlastest = SysRecipe::where('deleted_at','=',NULL)
                                        ->where('status', '=', 2)
                                        ->orderby('id','desc')
                                        ->limit(5)
                                        ->get();
        
        if(count($recipelistlastest) > 0 )
        {
            $status       = true;
            $status_code  = 200;
            $message      = "data found";
            $data         = $recipelistlastest;
        }else {
            $status       = false;
            $status_code  = 404;
            $message      = "data not found";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data];

        return response()->json($resp);
    }

    public function getRecipeDetail(Request $request){

        $recipedetail = DB::table('sys_recipe')
                        ->join('master_province','master_province.id','=','sys_recipe.province')
                        ->join('master_city','master_city.id','=','sys_recipe.city')
                        ->join('sys_category_recipe','sys_category_recipe.id','=','sys_recipe.recipe_category')
                        ->select('sys_recipe.id','sys_recipe.user_id','sys_recipe.recipe_name','sys_category_recipe.category_name as recipe_category','sys_recipe.description','sys_recipe.image','sys_recipe.gallery','sys_recipe.video','sys_recipe.inggridients','sys_recipe.cookmethd','sys_recipe.notes','master_province.name as province','master_city.name as city','sys_recipe.status','sys_recipe.views','sys_recipe.whislist','sys_recipe.share','sys_recipe.created_at','sys_recipe.updated_at','sys_recipe.deleted_at')
                        ->where('sys_recipe.id', '=', $request->input('recipe_id'))
                        ->where('sys_recipe.deleted_at','=',NULL)
                        ->where('sys_recipe.status', '=', 2)
                        ->first();
                    
        
        if(count($recipedetail) > 0 )
        {
            $status       = true;
            $status_code  = 200;
            $message      = "data found";
            $data         = $recipedetail;
        }else {
            $status       = false;
            $status_code  = 404;
            $message      = "data not found";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data];

        return response()->json($resp);
    }

    public function postRecipe(Request $request){
        
        $token = $request->header('Authorization');

        if($token == ""){
            $status       = false;
            $status_code  = 401;
            $message      = "Unauthorized";
            $data         = null;
        }else{
            $userid = $request->input('user_id');
    
            $chcktoken = SysToken::where('user_id', '=', $userid)
                                ->where('token', '=', $token)
                                ->get();
    
            if(count($chcktoken) > 0){
                $user_id	    = $request->input("user_id");
                $recipe_name    = $request->input("recipe_name");
                $recipe_category= $request->input("recipe_category");
                $description	= $request->input("description");
                $image	        = $request->file("image");
                $video          = $request->input("video");
                $ingredients    = stripslashes(preg_replace('/[^(\x20-\x7F)]*/','', $request->input("ingredients")));
                $cookmethd      = stripslashes(preg_replace('/[^(\x20-\x7F)]*/','', $request->input("cookmethd")));
                $notes          = $request->input("notes");
                $province       = $request->input("province");
                $city           = $request->input("city");
                $views          = 0;
                $wishlist       = 0;
                $share          = 0;
                $now            = new DateTime();
                $status         = 1;
            
                //========= Parsing Inggredient Json to array to string In One Field =========

                    $a = json_decode($ingredients, true);
                    $d = $a['ingredientsList'];

                    foreach($d as $b)
                    {
                        $cc = $b['title_ing'];
                        $ff = $b['mListIngDao'];

                        $list[] =[
                                'title'=>$cc,
                                'ingredients'=>$ff
                                ];

                        $results = array();
                        foreach($list as $key => $array)
                        {
                            foreach($array as $title => $value)
                            {
                            $results[$title] = $value;
                            }
                        }
                    $hasil[] = array_merge_recursive($results);
                    }

                    $str = "";
                    foreach($hasil as $val) {
                        foreach($val as $val2) {
                            if (!is_array($val2)) {
                                $str .= "<h3>".$val2 ."</h3>". " ";
                            } else {
                                foreach($val2 as $val3) {
                                    $str .="<ul><li>".$val3."</li></ul>". " ";
                                }
                            }
                        }
                    }

                //========================================================================

                //========= Parsing Steps Json to array to string In One Field =======

                    $ss = json_decode($cookmethd, true);
                    $ds = $ss['stepsList'];

                    foreach($ds as $bs)
                    {
                        $ccs = $bs['title_step'];
                        $ffs = $bs['mListIngDao'];

                        $liststep[] =[
                            'title'=>$ccs,
                            'steps'=>$ffs
                        ];

                    $resultstep = array();
                    foreach($liststep as $keystep => $arrays)
                    {
                        foreach($arrays as $titles => $values)
                        {
                            $resultstep[$titles] = $values;
                        }
                    }
                        $hasils[] = array_merge_recursive($resultstep);
                    }

                    $strs = "";
                    foreach($hasils as $vals) {
                        foreach($vals as $val2s) {
                            if (!is_array($val2s)) {
                            $strs .= "<h3>".$val2s ."</h3>". " ";
                            } else {
                                foreach($val2s as $val3s) {
                                    $strs .= "<ul><li>".$val3s ."</ul></li>". " ";
                                }
                                }
                            }
                            }
                //========================================================================


                if($image != "")
                    {
                        $upload = env('UPLOAD_PATH_RECIPE');
                        $image_name = time()."."."jpg";
                        $success = $image->move($upload,$image_name);
                        $IMGurl = 'https://theangkringan.harmonydshine.web.id/assets/apiv1/recipe/'.$image_name;
                    }else{
                        $IMGurl = 'https://theangkringan.harmonydshine.web.id/assets/apiv1/recipe/imagesno.jpeg';
                    }

            
                if($user_id == "" || $recipe_name == "" || $recipe_category == "" || $description == "" || $ingredients == "" || $cookmethd == "" || $province == "" || $city == "")
                {
                    $status       = false;
                    $status_code  = 402;
                    $message      = "Field can not be null";
                    $data         = null;
                }
                else
                {
                    $sysid = SysRecipe::create([
                        'user_id' => $user_id,
                        'recipe_name' => $recipe_name,
                        'recipe_category' => $recipe_category,
                        'description' => $description,
                        'image' => $IMGurl,
                        'gallery' => $IMGurl,
                        'video' => $video,
                        'inggridients' => $str,
                        'cookmethd' => $strs,
                        'notes' => $notes,
                        'province' => $province,
                        'city' => $city,
                        'status' => $status,
                        'views' => $views,
                        'whislist' => $wishlist,
                        'share' => $share,
                    ]);
                    DB::commit();

                    $retdata = array(
                        'recipe_id'	=> $sysid->id,
                        'recipe_name' => $recipe_name
                    );

                    $status       = true;
                    $status_code  = 200;
                    $message      = "data found";
                    $data         = $retdata;
                }
            }else{
                $status       = false;
                $status_code  = 401;
                $message      = "Unauthenticated";
                $data         = null;
            }

        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);
    }

    public function deleterecipe(Request $request)
    {
        $token = $request->header('Authorization');

        if($token == ""){
            $status       = false;
            $status_code  = 401;
            $message      = "Unauthorized";
            $data         = null;
        }else{
            $userid = $request->input('user_id');
    
            $chcktoken = SysToken::where('user_id', '=', $userid)
                                ->where('user_type', '=', 'member')
                                ->where('token', '=', $token)
                                ->get();
    
            if(count($chcktoken) > 0){
                if($object = SysRecipe::find($request->input('recipe_id'))){
                    $object->delete();   
                    $status       = true;
                    $status_code  = 200;
                    $message      = "Delete successfully";
                    $data         = null;
                }else{
                    $status       = false;
                    $status_code  = 404;
                    $message      = "Data Not Found";
                    $data         = null;
                }
            }else{
                $status       = false;
                $status_code  = 401;
                $message      = "Unauthenticated";
                $data         = null;
            }

        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);
    }

    public function updaterecipe(Request $request) {
        
        $token = $request->header('Authorization');

        if($token == ""){
            $status       = false;
            $status_code  = 401;
            $message      = "Unauthorized";
            $data         = null;
        }else{
            $userid = $request->input('user_id');
    
            $chcktoken = SysToken::where('user_id', '=', $userid)
                                ->where('user_type', '=', 'member')
                                ->where('token', '=', $token)
                                ->get();
    
            if(count($chcktoken) > 0){
                $user_id	    = $request->input("user_id");
                $recipe_id	    = $request->input("recipe_id");
                $recipe_name    = $request->input("recipe_name");
                $recipe_category= $request->input("recipe_category");
                $description	= $request->input("description");
                $image	        = $request->file("image");
                $video          = $request->input("video");
                $ingredients    = $request->input("ingredients");
                $cookmethd      = $request->input("cookmethd");
                $notes          = $request->input("notes");
                $province       = $request->input("province");
                $city           = $request->input("city");
                $now            = new DateTime();

                //========= Parsing Inggredient Json to array to string In One Field =========

                    $a = json_decode($ingredients, true);
                    $d = $a['ingredientsList'];

                    foreach($d as $b)
                    {
                        $cc = $b['title_ing'];
                        $ff = $b['mListIngDao'];

                        $list[] =[
                                'title'=>$cc,
                                'ingredients'=>$ff
                                ];

                        $results = array();
                        foreach($list as $key => $array)
                        {
                            foreach($array as $title => $value)
                            {
                            $results[$title] = $value;
                            }
                        }
                    $hasil[] = array_merge_recursive($results);
                    }

                    $str = "";
                    foreach($hasil as $val) {
                        foreach($val as $val2) {
                            if (!is_array($val2)) {
                                $str .= "<h3>".$val2 ."</h3>". " ";
                            } else {
                                foreach($val2 as $val3) {
                                    $str .="<ul><li>".$val3."</li></ul>". " ";
                                }
                            }
                        }
                    }

                //========================================================================

                //========= Parsing Steps Json to array to string In One Field =======

                    $ss = json_decode($cookmethd, true);
                    $ds = $ss['stepsList'];

                    foreach($ds as $bs)
                    {
                        $ccs = $bs['title_step'];
                        $ffs = $bs['mListIngDao'];

                        $liststep[] =[
                            'title'=>$ccs,
                            'steps'=>$ffs
                        ];

                    $resultstep = array();
                    foreach($liststep as $keystep => $arrays)
                    {
                        foreach($arrays as $titles => $values)
                        {
                            $resultstep[$titles] = $values;
                        }
                    }
                        $hasils[] = array_merge_recursive($resultstep);
                    }

                    $strs = "";
                    foreach($hasils as $vals) {
                        foreach($vals as $val2s) {
                            if (!is_array($val2s)) {
                            $strs .= "<h3>".$val2s ."</h3>". " ";
                            } else {
                                foreach($val2s as $val3s) {
                                    $strs .= "<ul><li>".$val3s ."</ul></li>". " ";
                                }
                                }
                            }
                            }
                //========================================================================

                $recipelist = SysRecipe::find($recipe_id);

                if($recipelist)
                {
                    if($image != "")
                    {
                        $upload = env('UPLOAD_PATH_RECIPE');
                        $image_name = time()."."."jpg";
                        $success = $image->move($upload,$image_name);
                        $IMGurl = 'https://theangkringan.harmonydshine.web.id/assets/apiv1/recipe/'.$image_name;
                    }else{
                        $IMGurl = $recipelist->image;
                    }

                    $recipelist->recipe_name = $recipe_name;
                    $recipelist->recipe_category = $recipe_category;
                    $recipelist->description = $description;
                    $recipelist->image = $IMGurl;
                    $recipelist->gallery = $IMGurl;
                    $recipelist->video = $video;
                    $recipelist->inggridients = $str;
                    $recipelist->cookmethd = $strs;
                    $recipelist->notes = $notes;
                    $recipelist->province = $province;
                    $recipelist->city = $city;

                    if($recipelist->save()){    

                        $retdata = array(
                            'user_id'	=> $request->input('user_id'),
                            'recipe_id'	=> $request->input('recipe_id'),
                            'recipe_name'	=> $request->input('recipe_name')
                        );

                        $status       = true;
                        $status_code  = 200;
                        $message      = "Data updated successfully ";
                        $data         = $retdata;
                    }
                }
                else
                {
                    $status       = false;
                    $status_code  = 404;
                    $message      = "Data not found";
                    $data         = null;
                }
            }else{
                $status       = false;
                $status_code  = 401;
                $message      = "Unauthenticated";
                $data         = null;
            }

        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);
    }

    public function changeRecipeStatus(Request $request){
        
        $token = $request->header('Authorization');

        if($token == ""){
            $status       = false;
            $status_code  = 401;
            $message      = "Unauthorized";
            $data         = null;
        }else{
            $userid = $request->input('user_id');
    
            $chcktoken = SysToken::where('user_id', '=', $userid)
                                ->where('user_type', '=', 'admin')
                                ->where('token', '=', $token)
                                ->get();
    
            if(count($chcktoken) > 0){
                $recipe = SysRecipe::find($request->input('recipe_id'));
                $recipe->status = $request->input('status');
                if($recipe->save()){
                    $user_data = SysMembers::find($recipe->user_id);
                    if($request->input('status') == 2){
                        $subject = 'Congratulations, Recipe '.ucwords($recipe->recipe_name).' is accepted';
                        $mail_view = 'mail_accepted';
                    }else{
                        $subject = 'Sorry, Recipe '.ucwords($recipe->recipe_name).' is rejected';
                        $mail_view = 'mail_rejected';
                    }

                    $data = array(
                        'email'=> $user_data->email, 
                        'fullname'=> $user_data->fullname, 
                        'subject' => $subject
                    );
                
                    Mail::send($mail_view, $data, function($message) use ($data) {
                        $message->to($data['email'])
                                ->from('theangkringanproject@gmail.com', 'The Angkringan')
                                ->subject($data['subject']);
            
                    });
            
                    if (Mail::failures()) {
                        
                    }else{
                        $status       = true;
                        $status_code  = 200;
                        $message      = "Status changed successfully";
                        $data         = null;
                    }
                }else{
                    $status       = false;
                    $status_code  = 400;
                    $message      = "Faile to change status. Please try again";
                    $data         = null;
                }
            }else{
                $status       = false;
                $status_code  = 401;
                $message      = "Unauthenticated";
                $data         = null;
            }

        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);

    }

    public function searchRecipe(Request $request){
        $recipe = SysRecipe::where('recipe_name', 'like', '%'.$request->input('string').'%')
                                        ->where('status', '=', 2)
                                        ->get();
        
        if(count($recipe) > 0 )
        {
            $status       = true;
            $status_code  = 200;
            $message      = "data found";
            $data         = $recipe;
        }else {
            $status       = false;
            $status_code  = 404;
            $message      = "data not found";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data];

        return response()->json($resp);
    }

    public function addWishlist(Request $request){
        
        $token = $request->header('Authorization');

        if($token == ""){
            $status       = false;
            $status_code  = 401;
            $message      = "Unauthorized";
            $data         = null;
        }else{
            $userid = $request->input('user_id');
    
            $chcktoken = SysToken::where('user_id', '=', $userid)
                                ->where('user_type', '=', 'member')
                                ->where('token', '=', $token)
                                ->get();
    
            if(count($chcktoken) > 0){
                $member = SysMembers::find($userid);

                $cekwhs = SysWishlist::where('member_id', $userid)->where('recipe_id',$request->input('recipe_id'))->get();

                if(count($cekwhs) > 0)
                {
                    $status       = false;
                    $status_code  = 404;
                    $message      = "already in whislist";
                    $data         = null;
                }
                else
                {
                    $syswish = SysWishlist::create([
                        'member_id' => $userid,
                        'fullname' => $member->fullname,
                        'recipe_id' => $request->input('recipe_id'),
                        'status' => 1
                    ]);
                    DB::commit();
    
                    $retdata = array(
                        'wishlist_id'	=> $syswish->id
                    );
    
                    $status       = true;
                    $status_code  = 200;
                    $message      = "data found";
                    $data         = $retdata;
                }
            }else{
                $status       = false;
                $status_code  = 401;
                $message      = "Unauthenticated";
                $data         = null;
            }

        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);

    }

    public function getWishlist(Request $request){
        $token = $request->header('Authorization');

        if($token == ""){
            $status       = false;
            $status_code  = 401;
            $message      = "Unauthorized";
            $data         = null;
        }else{
            $userid = $request->input('user_id');
    
            $chcktoken = SysToken::where('user_id', '=', $userid)
                                ->where('user_type', '=', 'member')
                                ->where('token', '=', $token)
                                ->get();
    
            if(count($chcktoken) > 0){
                $wishlist = DB::table('sys_whislist')
                            ->join('sys_recipe','sys_recipe.id','=','sys_whislist.recipe_id')
                            ->select('sys_whislist.id as whislist_id','sys_whislist.member_id','sys_whislist.fullname','sys_recipe.*')
                            ->where('sys_whislist.member_id', '=', $request->input('user_id'))
                            ->where('sys_whislist.status', '=', 1)
                            ->get();
                            
                if(count($wishlist) > 0 )
                {
                    $status       = true;
                    $status_code  = 200;
                    $message      = "data found";
                    $data         = $wishlist;
                }else {
                    $status       = false;
                    $status_code  = 404;
                    $message      = "data not found";
                    $data         = null;
                }
            }else{
                $status       = false;
                $status_code  = 401;
                $message      = "Unauthenticated";
                $data         = null;
            }
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data];

        return response()->json($resp);
    }

    public function deleteWishlist(Request $request)
    {
        $token = $request->header('Authorization');

        if($token == ""){
            $status       = false;
            $status_code  = 401;
            $message      = "Unauthorized";
            $data         = null;
        }else{
            $userid = $request->input('user_id');
    
            $chcktoken = SysToken::where('user_id', '=', $userid)
                                ->where('user_type', '=', 'member')
                                ->where('token', '=', $token)
                                ->get();
    
            if(count($chcktoken) > 0){
                if($object = SysWishlist::where('recipe_id', $request->input('recipe_id'))){
                    $object->delete();   
                    $status       = true;
                    $status_code  = 200;
                    $message      = "Delete successfully";
                    $data         = null;
                }else{
                    $status       = false;
                    $status_code  = 404;
                    $message      = "Data Not Found";
                    $data         = null;
                }
            }else{
                $status       = false;
                $status_code  = 401;
                $message      = "Unauthenticated";
                $data         = null;
            }

        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);
    }

    public function addReview(Request $request){
        
        $token = $request->header('Authorization');

        if($token == ""){
            $status       = false;
            $status_code  = 401;
            $message      = "Unauthorized";
            $data         = null;
        }else{
            $userid = $request->input('user_id');
            $recipeid = $request->input('recipe_id');
            $comment = $request->input('comment');
            $rating = $request->input('rating');
    
            $chcktoken = SysToken::where('user_id', '=', $userid)
                                ->where('user_type', '=', 'member')
                                ->where('token', '=', $token)
                                ->get();
    
            if(count($chcktoken) > 0){
                $chkrating = SysReview::where('user_id', '=', $userid)
                                        ->where('recipe_id', '=', $recipeid)
                                        ->get();
                if(count($chkrating) > 0){
                    $status       = false;
                    $status_code  = 400;
                    $message      = "You have already reviewed this recipe";
                    $data         = null;

                }else{
                    $member = SysMembers::find($userid);
                    $sysrev = SysReview::create([
                        'user_id' => $userid,
                        'recipe_id' => $recipeid,
                        'fullname' => $member->fullname,
                        'email' => $member->email,
                        'rating' => $rating,
                        'comment' => $comment,
                        'status' => 1
                    ]);
                    DB::commit();
    
                    $retdata = array(
                        'review_id'	=> $sysrev->id
                    );
    
                    $status       = true;
                    $status_code  = 200;
                    $message      = "data found";
                    $data         = $retdata;
                }
            }else{
                $status       = false;
                $status_code  = 401;
                $message      = "Unauthenticated";
                $data         = null;
            }

        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data
        ];

        return response()->json($resp);

    }

    public function getReview(Request $request){
        $review = SysReview::where('sys_reviews.recipe_id', '=', $request->input('recipe_id'))
                                ->where('sys_reviews.status', '=', 1)
                                ->get();
        
        if(count($review) > 0 )
        {
            $status       = true;
            $status_code  = 200;
            $message      = "data found";
            $data         = $review;
        }else {
            $status       = false;
            $status_code  = 404;
            $message      = "data not found";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data];

        return response()->json($resp);
    }

    public function getTotalRating(Request $request){
            $reviewdata = SysReview::where('sys_reviews.recipe_id', '=', $request->input('recipe_id'))
                        ->where('sys_reviews.status', '=', 1)
                        ->get();

            if(count($reviewdata) > 0)
            {
                $countreview = DB::table('sys_reviews')->where('recipe_id','=',$request->input('recipe_id'))->where('status','=',1)->count();
                $totalrating = DB::table('sys_reviews')->where('recipe_id','=',$request->input('recipe_id'))->where('status','=',1)->sum('rating');
                $avgrating = ceil($totalrating/$countreview);
                $convertavgrate = intval($avgrating);
                $sumcomment = count($reviewdata);
            }
            else
            {
                $avgrating = 0;
                $convertavgrate = intval($avgrating);
                $sumcomment = 0;
            }

        $ratingtotal = ['recipe_id' => $request->input('recipe_id'),
                        'totalrating' => $convertavgrate,
                        'totalcomment' => $sumcomment,
                    ];
        
        if(count($ratingtotal) > 0 )
        {
            $status       = true;
            $status_code  = 200;
            $message      = "data found";
            $data         = $ratingtotal;
        }else {
            $status       = false;
            $status_code  = 404;
            $message      = "data not found";
            $data         = null;
        }

        $resp = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'data'  => $data];

        return response()->json($resp);
    }
}
