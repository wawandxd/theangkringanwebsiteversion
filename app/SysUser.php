<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SysUser extends Model
{
    use SoftDeletes;

    protected $table = 'sys_users';

    protected $fillable = ['role_id', 'fullname', 'email', 'address', 'phone', 'gender', 'password', 'password', 'avatar', 'status', 'created_at', 'updated_at', 'remember_token'];

    protected $dates = ['deleted_at'];
}
