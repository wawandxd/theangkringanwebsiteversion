<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SysCity extends Model
{
    use SoftDeletes;

    protected $table = 'master_city';

    protected $dates = ['deleted_at'];
}
