<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysReview extends Model
{
    protected $table = 'sys_reviews';

    protected $fillable = ['user_id', 'recipe_id', 'fullname', 'email', 'comment', 'rating', 'status'];
}
