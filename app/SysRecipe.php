<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SysRecipe extends Model
{
    use SoftDeletes;

    protected $table = 'sys_recipe';

    protected $fillable = [
        'user_id',
        'recipe_name',
        'recipe_category',
        'description',
        'image',
        'gallery',
        'video',
        'inggridients',
        'cookmethd',
        'notes',
        'province',
        'city',
        'status',
        'views',
        'whislist',
        'share'];

    protected $dates = ['deleted_at'];
}
