<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysWishlist extends Model
{
    protected $table = 'sys_whislist';

    protected $fillable = ['member_id', 'fullname', 'recipe_id', 'status'];
}
