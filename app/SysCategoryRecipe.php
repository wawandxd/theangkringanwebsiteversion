<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SysCategoryRecipe extends Model
{
    use SoftDeletes;

    protected $table = 'sys_category_recipe';

    protected $fillable = ['category_name', 'description', 'image', 'status'];

    protected $dates = ['deleted_at'];
}
